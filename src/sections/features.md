---
title: Ejes de trabajo
afterTitle: 
type: features
---

Los ejes de trabajo que nos aglutina en este proceso de construccion de la cátedra son preguntas, no solo porque la pregunta es la herramienta que mejor refleja el momento actual que estamos compartiendo, tambien porque  resalta el caracter reflexivo, dinámico, abierto, crítico y circular de los intercambios de saberes y prácticas que intentamos impulsar y habitar. 
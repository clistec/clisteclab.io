---
title: Sobre la Cátedra Libre
type: about
---

La cátedra libre de Soberanía Tecnológica, en la **Universidad Nacional de Río Cuarto**, es un proyecto en formación que reúne a una diversidad de personas y comunidades, de dentro y fuera de la academia, con el fin de producir un espacio transdisciplinario en la universidad que pueda ser habitado no solo por academicos, tecnicxs y especialistas, sino principalmente por aquellas comunidades productoras de tecnologias populares y situadas. Uno de nuestros objetivos es la problematización de los modelos cerrados y centralizados de producción tecnológica, generalmente hegemónicos,  que habitualmente deja a los y las usuarios/as sin la posibilidad de produccion de decisiones y de control de las infraestructuras en torno a las tecnologias. Tambien nos interesa visibilizar y articular alternativas locales, populares, feministas, que desde la práctica proponen nuevas formas de desarrollo tecnológico. Nos proponemos promover la discusión y el intercambio de experiencias en torno al uso y elección de tecnologías.

# Ejes de trabajo

Los ejes de trabajo que nos aglutina en este proceso de construccion de la cátedra son preguntas, no solo porque la pregunta es la herramienta que mejor refleja el momento actual que estamos compartiendo, tambien porque  resalta el caracter reflexivo, dinámico, abierto, crítico y circular de los intercambios de saberes y prácticas que intentamos impulsar y habitar. 